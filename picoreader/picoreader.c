/*******************************************************************************
 *
 *  Filename: picohrdlCon.c
 *
 *  Description:
 *		This is a console-mode program that demonstrates how to use the
 *		picohrdl driver API functions for the PicoLog ADC-20 and ADC-24 
 *		High Resolution Data Loggers.
 *
 *  There are five examples:
 *		Collect a block of samples immediately
 *		Collect a block of samples when a trigger event occurs
 *		Use windowing to collect a sequence of overlapped blocks
 *		Write a continuous stream of data to a disk file
 *		Take individual readings
 *
 *	To build this application:-
 *
 *		If Microsoft Visual Studio (including Express) is being used:
 *
 *			Select the solution configuration (Debug/Release) and platform (x86/x64)
 *			Ensure that the 32-/64-bit picohrdl.lib can be located
 *			Ensure that the HRDL.h file can be located
 *
 *		Otherwise:
 *
 *			Set up a project for a 32-/64-bit console mode application
 *			Add this file to the project
 *			Add the appropriate 32-/64-bit picohrdl.lib to the project (Microsoft C only)
 *			Build the project
 *
 *  Linux platforms:
 *
 *		Ensure that the libpicohrdl driver package has been installed using the
 *		instructions from https://www.picotech.com/downloads/linux
 *
 *		Place this file in the same folder as the files from the linux-build-files
 *		folder. In a terminal window, use the following commands to build
 *		the picohrdlCon application:
 *
 *			./autogen.sh <ENTER>
 *			make <ENTER>
 *
 *  Copyright � 2004-2018 Pico Technology Ltd. See LICENSE file for terms.
 *
 ******************************************************************************/
#include <stdio.h>
#include <math.h>
#include <time.h>

#ifdef WIN32
#include <conio.h>
#include <windows.h>
#include "HRDL.h"

#else
#include <sys/types.h>
#include <string.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>

#include "libpicohrdl-1.0/HRDL.h"
#include "undergrowth_api.h"

#define Sleep(a) usleep(1000*a)
#define scanf_s scanf
#define fscanf_s fscanf
#define memcpy_s(a,b,c,d) memcpy(a,c,d)

typedef enum enBOOL{FALSE,TRUE} BOOL;

/* A function to detect a keyboard press on Linux */
int32_t _getch()
{
        struct termios oldt, newt;
        int32_t ch;
        int32_t bytesWaiting;
        tcgetattr(STDIN_FILENO, &oldt);
        newt = oldt;
        newt.c_lflag &= ~( ICANON | ECHO );
        tcsetattr(STDIN_FILENO, TCSANOW, &newt);
        setbuf(stdin, NULL);
        do {
                ioctl(STDIN_FILENO, FIONREAD, &bytesWaiting);
                if (bytesWaiting)
                        getchar();
        } while (bytesWaiting);

        ch = getchar();

        tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
        return ch;
}

int32_t _kbhit()
{
        struct termios oldt, newt;
        int32_t bytesWaiting;
        tcgetattr(STDIN_FILENO, &oldt);
        newt = oldt;
        newt.c_lflag &= ~( ICANON | ECHO );
        tcsetattr(STDIN_FILENO, TCSANOW, &newt);
        setbuf(stdin, NULL);
        ioctl(STDIN_FILENO, FIONREAD, &bytesWaiting);

        tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
        return bytesWaiting;
}

int32_t fopen_s(FILE ** a, const char * b, const char * c)
{
FILE * fp = fopen(b,c);
*a = fp;
return (fp>0)?0:-1;
}

/* A function to get a single character on Linux */
#define max(a,b) ((a) > (b) ? a : b)
#define min(a,b) ((a) < (b) ? a : b)
#endif

/* Globals */

struct structChannelSettings 
{
	int16_t enabled;
	HRDL_RANGE range;
	int16_t singleEnded;
} g_channelSettings[HRDL_MAX_ANALOG_CHANNELS + 1];

int16_t		g_device;
int16_t		g_maxNoOfChannels;
int32_t		g_scaleTo_mv;

double inputRangeDivider [] = {1, 2, 4, 8, 16, 32, 64}; // Used for different voltage scales

/****************************************************************************/

/****************************************************************************
*
* OpenDevice
*  this function demonstrates how to open the next available unit
*
****************************************************************************/
int16_t OpenDevice(int16_t async)
{
	int16_t device = 0;

	//
	// Start the opening routine, this will block until the 
	// device open routine completes
	//
	device = HRDLOpenUnit();    

	return device;
}

/****************************************************************************
*
* SelectUnit
*	This function demonstrates how to open all available units and
*	select the required one.
*
****************************************************************************/
int16_t SelectUnit(void)
{
	int16_t device;
	int8_t line[80];

	int16_t async; 
	int16_t i;   

	int16_t deviceToUse = 0;
	int16_t ndevicesFound = 0;     

	printf("\n\nOpening devices synchronously..."); 
	async = (int16_t) FALSE;

	printf ("\n\nOpening devices.\n");
	

	device = OpenDevice(async);

	//
	// If the device is available give the user the option of using it
	//
	if (device > 0)
	{
		HRDLGetUnitInfo(device, line, sizeof (line), HRDL_BATCH_AND_SERIAL);
		printf("Device: %s\n", line);  
		return device;       
	}
	else
	{
		HRDLGetUnitInfo(device, line, sizeof (line), HRDL_ERROR);

		if (atoi(line) == HRDL_NOT_FOUND)
		{
			printf("Device Unavailable: No Unit Found\n", i + 1);               
		}
		else
		{
			printf("Device Unavailable: %s\n", line);               
		}
		return 0;
	}

} 

/****************************************************************************
*
* SetAnalogChannels
*  This function demonstrates how to detect available input range and set it.
*  We will first check to see if a channel is available, then check what ranges
*  are available and then check to see if differential mode is supported for thet
*  channel.
*
****************************************************************************/
void SetAnalogChannels(void)
{ 
	int32_t channel;
	int16_t range;
	int16_t available;
	int16_t status = 1;

	printf("\n");

	//
	// Iterate through the channels on the device and if one is available give the
	// user the option of using one;
	//
	// A channel may not be available if:
	//
	// a) it is not available on the current device,
	// b) the input is a secondary differential input and is already in use for a differential channel or
	// c) the input is a primary differential input and cannot be used for a differential channel because the
	//    secondary input of the pair is already in use for a single-ended channel.
	//
	// Primary inputs for differential pairs are odd channel numbers eg  1, 3, 5, etc. Their corresponding
	// secondary numbers are primary channel number + 1 eg 1 and 2, 3 and 4, etc.
	//
	// You should firstly make sure that the channel is available on the current unit
	// and secondly ensure that the input, or both inputs for a differential channel,
	// are not already in use for another channel.


	for (channel = HRDL_ANALOG_IN_CHANNEL_1; channel <= g_maxNoOfChannels; channel++)
	{
		printf("%2d - Channel %d\n", channel, channel); 
	}

	//
	// Let the user select the channel

	printf("Select a channel..\n");
	for (int channel = 1; channel <= g_maxNoOfChannels; channel++)
	{
		//channel = 1;
	  
		if (channel < HRDL_ANALOG_IN_CHANNEL_1 || channel > g_maxNoOfChannels)
		{
			printf ("Error with channel number");
			return;
		}
	
		printf("Enabling channel %d\n", channel);  
		g_channelSettings[channel].enabled = (int16_t)TRUE;//(int16_t) toupper(_getch()) == 'Y';
	
		//
		// Disable the channel if the user does not require it
	
		if (!g_channelSettings[channel].enabled)
		{
			printf("Channel %d disabled\n\n", channel);
			HRDLSetAnalogInChannel(g_device, (int16_t)channel, (int16_t) 0, (int16_t) HRDL_1250_MV, (int16_t) 1);
			return;
		}          
	
		//
		// Iterate through all the input ranges, if the range is available on
		// the current device, give the user the option of using it.
	
		available = 0;
	
		for (range = 0; range < HRDL_MAX_RANGES; range++)
		{
			status = HRDLSetAnalogInChannel(g_device, (int16_t) channel, (int16_t) 1, (int16_t) range, (int16_t) 1);
	
			if (status == 1)
			{
				printf("%d - %d mV\n", range, (int32_t) round(2500.0 / (double) inputRangeDivider[range]));
				available = 1;
			}
		}
	
		if (!available)
		{
			printf("Channel is not available for use:\n ");
			
			if ((channel & 0x01) && g_channelSettings[channel + 1].enabled )  // odd channels
			{
				printf("The channel cannot be enabled because it is a primary differential channel  \
							and its corresponding secondary channel is already in use for a single-ended measurement\n");           
			}
			else if (g_channelSettings[channel - 1].enabled)
			{
				printf("The channel cannot be enabled because it is a secondary differential channel  \
							and is already in use for a differential measurement\n");
			}
			else
			{
				printf("This channel cannot be enabled because it is not available on this Pico HRDL variant\n");
			}                                         
			return;
		}
	
		//
		// Let the user select the range
		printf("Setting range for input...\n");
		
		do 
		{
			//g_channelSettings[channel].range = (HRDL_RANGE) (_getch() - '0');
			if (channel < 5)
			{
				g_channelSettings[channel].range = (HRDL_RANGE) (0);
				printf ("Channel %d - range 0\n", channel);
			}
			else 
			{
				g_channelSettings[channel].range = (HRDL_RANGE) (1);
				printf ("Channel %d - range 1\n", channel);
			}
	
		} while (!HRDLSetAnalogInChannel(g_device, (int16_t) channel, (int16_t) 1, (int16_t) g_channelSettings[channel].range, (int16_t) 1));
	
		//
		// See if it possible to use this channel as differential.
		// 
		// It may not be used as differential if this input is a secondary differential input
		// or this input is a primary differential input and the corresponding secondary input
		// is already in use for a single-ended channel.
	
		if (HRDLSetAnalogInChannel(g_device, (int16_t)channel, (int16_t) 1, (int16_t) g_channelSettings[channel].range, (int16_t) 0))
		{
			printf("Setting input as single-ended");  
			//g_channelSettings[channel].singleEnded = (int16_t) toupper(_getch()) == 'Y';
			g_channelSettings[channel].singleEnded = 1;
		}
		else
		{
			g_channelSettings[channel].singleEnded = 1;           
		}
	
		HRDLSetAnalogInChannel(g_device, (int16_t) channel, (int16_t) 1, (int16_t) g_channelSettings[channel].range, g_channelSettings[channel].singleEnded);
	
		// Let the user know what they have set
		printf("\nChannel %d: %d mV range, %s\n\n", channel, (int32_t) round(2500.0 / (double) inputRangeDivider[g_channelSettings[channel].range]), g_channelSettings[channel].singleEnded ? "single-ended" : "differential");
	}
}

/****************************************************************************
*
* AdcTo_mv
*
* If the user selects scaling to millivolts,
* Convert an ADC count into millivolts
*
****************************************************************************/
float AdcToMv (HRDL_INPUTS channel, int32_t raw)
{ 
	int32_t maxAdc = 0;
	int32_t minAdc = 0;

	if (channel < HRDL_ANALOG_IN_CHANNEL_1 || channel > HRDL_MAX_ANALOG_CHANNELS)
	{
		return 0;
	}

	if (raw == -1)
	{
		return -1;
	}

	HRDLGetMinMaxAdcCounts(g_device, &minAdc, &maxAdc, channel);

	// To convert from adc to V you need to use the following equation
	//            maxV - minV
	//   raw =  ---------------
	//          maxAdc - minAdc
	//
	// if we assume that V and adc counts are bipolar and symmetrical about 0, the 
	// equation reduces to the following:
	//            maxV
	//   raw =  --------
	//           maxAdc 
	//
    
	//
	// Note the use of the maxAdc count for the HRDL in the equation below:  
	//
	// maxAdc is always 1 adc count short of the advertised full voltage scale
	// minAdc is always exactly the advertised minimum voltage scale.
	//
	// It is this way to ensure that we have an adc value that
	// equates to exactly zero volts.
	//
	// maxAdc     == maxV
	// 0 adc      == 0 V
	// minAdc     == minV
	// 

	if (g_scaleTo_mv)
	{
		return (float)  ((double) raw * (2500.0 / pow(2.0, (double) g_channelSettings[channel].range)) / (double)(maxAdc));
	}
	else
	{
		return (float) raw;
	}

}

#define SamplesPerReading 6 * 15 //six readings per minute for 15 mins
int sampleCounter = 0;

typedef struct
{
    float samples[SamplesPerReading];
} MeanData;

MeanData meanData[8];//8 channels of mean data

void addReading (MeanData* meanData, int index, float readingToAdd)
{
    meanData->samples[index] = readingToAdd;
}

float calculateMean (MeanData* meanData)
{
    double sum = 0.0;
    int counter;
    for (counter = 0; counter < SamplesPerReading; counter++)
    {
		//printf ("Calulating Mean Sample:\t%d:\t%f\n", counter, meanData->samples[counter]);
        sum += (double) meanData->samples[counter];
    }
    const double denominator = (double)SamplesPerReading;
    //printf ("Sum = %lf\n", sum);
    //printf ("SamplesPerReading = %lf\n", denominator);
    return (float)(sum / denominator);
}

/****************************************************************************
*
* CollectSingle using blocking Api Calls
*	This function demonstrates how to collect analogue values one at a time.
*	This function also demonstrates how to set and discover what digital IO 
*	values.
*
*	In this mode, you can collect data and manage your own timing
*
****************************************************************************/
void CollectSingleBlocked (void)
{
	int16_t channel;
	int32_t value;
	int16_t status;

	
	int i = 0;

	//printf("\n");

	// Get the analogue input measurements
	for (channel = HRDL_ANALOG_IN_CHANNEL_1; channel <= HRDL_MAX_ANALOG_CHANNELS; channel++)
	{              
		if (!g_channelSettings[channel].enabled)
		{
			continue;
		}

		status = HRDLGetSingleValue(g_device, channel, g_channelSettings[channel].range, /*HRDL_660MS*/HRDL_60MS, g_channelSettings[channel].singleEnded, NULL, &value);

		if (!status)
		{
			//printf ("Channel %d not converted\n", channel); 
		}
		else
		{
			float f =  AdcToMv ((HRDL_INPUTS) channel, value);
			//printf ("%d\tChannel %d:\t%f\n", sampleCounter, channel, f); 
			addReading (&(meanData[i]), sampleCounter, f);
			
		}
		i++;
	}
	sampleCounter++;
	if (sampleCounter == SamplesPerReading)
	{
		struct Sample sample;
		sample.timestamp = 0;
		
		for (i = 0; i < 8; i++)
		{
			float f = calculateMean (&(meanData[i]));
			//printf ("Mean of channel %d:\t%f\n", i, f); 
			sample.values[i] = f;
		}
		
		put_sample(&sample);
		sampleCounter = 0;
	}
	
	

	if (g_channelSettings[HRDL_DIGITAL_CHANNELS].enabled)
	{
		// If digital IO is available on this unit, check the status of the inputs    
		if (HRDLGetSingleValue(g_device, HRDL_DIGITAL_CHANNELS, 0, 0, 0, NULL, &value))
		{
			printf("Digital Channel %d: %d\n", 1, (value & HRDL_DIGITAL_IO_CHANNEL_1) == HRDL_DIGITAL_IO_CHANNEL_1);
			printf("Digital Channel %d: %d\n", 2, (value & HRDL_DIGITAL_IO_CHANNEL_2) == HRDL_DIGITAL_IO_CHANNEL_2);
			printf("Digital Channel %d: %d\n", 3, (value & HRDL_DIGITAL_IO_CHANNEL_3) == HRDL_DIGITAL_IO_CHANNEL_3);
			printf("Digital Channel %d: %d\n", 4, (value & HRDL_DIGITAL_IO_CHANNEL_4) == HRDL_DIGITAL_IO_CHANNEL_4);
		}
	}
    
}


/****************************************************************************
*
*
****************************************************************************/
void main (void)
{ 
	int32_t		ok = 0;
	int8_t 		line [80];  
	int16_t		lineNo;

	int8_t		ch;

	int8_t description[7][25] = { "Driver Version    :", 
									"USB Version       :", 
									"Hardware Version  :", 
									"Variant Info      :", 
									"Batch and Serial  :", 
									"Calibration Date  :", 
									"Kernel Driver Ver.:"};

//	g_doSet = FALSE;
	printf("PicoLog High Resolution Data Logger (picohrdl) driver example program for ADC-20/24 data loggers\n");
  
	memset(g_channelSettings, 0, sizeof(g_channelSettings));

	g_device = SelectUnit();

	ok = g_device > 0;        


	if (!ok)
	{
		printf("Unable to open device\n");
		HRDLGetUnitInfo(g_device, line, sizeof (line), HRDL_ERROR);
		printf("%s\n", line);
		exit(99);
	}
	else
	{
		printf("Device opened successfully.\n\n");   
		printf("Device Information\n");
		printf("==================\n\n");

		//
		// Get all the information related to the device
		//
		for (lineNo = 0; lineNo < HRDL_ERROR; lineNo++)
		{
			HRDLGetUnitInfo(g_device, line, sizeof (line), lineNo);
				
			if (lineNo == HRDL_VARIANT_INFO)
			{
				switch(atoi(line))
				{
					case 20:
						g_maxNoOfChannels = 8;
						break;
					
					case 24:
						g_maxNoOfChannels = 16;
						break;
					
					default:
						printf("Invalid unit type returned from driver");
						return;
				}
			}
			
			if (lineNo == HRDL_VARIANT_INFO)
			{
				printf("%s ADC-%s\n", description[lineNo], line);
			}
			else
			{
				printf("%s %s\n", description[lineNo], line);
			}
		}

		printf("\n");

		//scale to mv set false to use ticks
		g_scaleTo_mv = TRUE;

		//Reject 50Hz mains noise, set to TRUE if you want this
		
		if (FALSE)
		{
			HRDLSetMains(g_device, 1);
		}
		else
		{ 
			HRDLSetMains(g_device, 0);
		}

		SetAnalogChannels();
		//for (int i = 0; i < 20; i++)
		for(;;)
		{
			CollectSingleBlocked();

			//time_t t = time(NULL);
			//struct tm tm = *localtime(&t);
			//FILE* fp;
			//fp = fopen ("log.txt", "a+");
			//fprintf(fp,"now: %d-%02d-%02d %02d:%02d:%02d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
			//fclose(fp);
			sleep(10);
		}

		HRDLCloseUnit(g_device);	
	}  
	 
}
